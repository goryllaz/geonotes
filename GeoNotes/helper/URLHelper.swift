//
//  URLHelper.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 24/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit

class URLHelper: NSObject {
    fileprivate static let primaryUrl: URLtype = .publicURL
    
    enum URLtype {
        case privateURL
        case publicURL
    }
    
    public static func getUrl(type:URLtype) -> String {
        switch type {
        case .privateURL:
            return "127.0.0.1"
        case .publicURL:
            return "80.211.93.212"
        }
    }
    
    public static func getPrimaryUrl() -> String {
        return getUrl(type: primaryUrl)
    }
}
