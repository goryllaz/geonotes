//
//  NoteMarker.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 25/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import GoogleMaps

class NoteMarker: GMSMarker {
    enum MarkerType {
        case mine
        case foreign
    }
    
    init(type: MarkerType, id: String, latitude: Double, longitude: Double) {
        super.init()
        position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let iconImage:String;
        switch type {
        case .mine:
            iconImage = "myMarker"
        case .foreign:
            iconImage = "marker"
        }
        icon = UIImage(named: iconImage)
        groundAnchor = CGPoint(x: 0.5, y: 0.5)
        var data = Dictionary<String, Any>()
        data["id"] = id
        userData = data
    }
}
