//
//  Note.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 25/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import Foundation
import RealmSwift

class Note: Object, Decodable {
    @objc dynamic var id: String?
    @objc dynamic var text: String?
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var dateCreate: Date?
    @objc dynamic var username: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
