//
//  CenterSymbol.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 23/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import Macaw

class CenterSymbol: MacawView {

    required init?(coder aDecoder: NSCoder) {
        let group = Group(place: .move(dx: 25, dy: 25))
        group.contentsVar.animation({ t in
            let color = Color.rgba(r: 210, g: 77, b: 87, a: 1 - t)
            return [Circle(r: t * 25).stroke(fill: color, width: 3)]
        }, during: 2, delay: 0.1).easing(.easeInOut).cycle().play()
        super.init(node: group, coder: aDecoder)
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}
