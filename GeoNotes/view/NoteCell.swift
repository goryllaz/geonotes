//
//  NoteCell.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 27/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {
    var note:Note?
    
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var mainTextLabel: UILabel!
    
    func setNoteCell(note: Note) {
        usernameLabel.text = note.username
        mainTextLabel.text = note.text
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
