//
//  DesignableView.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 18/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
