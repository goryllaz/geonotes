//
//  ViewController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 17/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit

class LoadingController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.object(forKey: "username") != nil &&
            UserDefaults.standard.object(forKey: "token") != nil {
            let mapController = self.storyboard?.instantiateViewController(withIdentifier: "GeoNoteNavigatonController") as! GeoNoteNavigatonController
            self.present(mapController, animated: true, completion: nil)
        } else {
            let registrationController = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationController") as! RegistrationController
            self.present(registrationController, animated: true, completion: nil)
        }
    }
}
