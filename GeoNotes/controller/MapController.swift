//
//  MapController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 21/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import RealmSwift

class MapController: UIViewController {
    
    fileprivate var lastLatitude: Double = 50.204261
    fileprivate var lastLongitude: Double = 15.829363
    fileprivate var timer = Timer()
    fileprivate var getNoteInterval = 5.0
    fileprivate let dist = 200.0;
    var isAlreadyAppeared: Bool = false
    
    fileprivate let locationManager = CLLocationManager()
    let kMapStyle = """
    [
        {
            "featureType": "poi",
            "stylers": [
              { "visibility": "off" }
            ]
        }
    ]
    """
    
    @IBOutlet fileprivate var mapView: GMSMapView!
    
    
    @IBAction func addNoteHandler(_ sender: Any) {
        UserDefaults.standard.set(lastLatitude, forKey: "lastAddLatitude")
        UserDefaults.standard.set(lastLongitude, forKey: "lastAddLongitude")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaultRealmPath = Realm.Configuration.defaultConfiguration.fileURL!
        print("Default Realm path: \(defaultRealmPath)")
        
        mapView.delegate = self
        locationManager.delegate = self
        
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        checkLocationAuthorization()
        
        let camera = GMSCameraPosition.camera(withLatitude: lastLatitude, longitude: lastLongitude, zoom: 16)
        mapView.camera = camera
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures   = false
        mapView.settings.tiltGestures   = false
        mapView.settings.rotateGestures = false
        
        do {
            mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        locationManager.startUpdatingLocation()
        
        //updateNotes()
        timer = Timer.scheduledTimer(timeInterval: getNoteInterval, target: self, selector: #selector(updateNotes), userInfo: nil, repeats: true)
    }
    
    func drawMarkers() {
        let realm = try! Realm()
        let latMin = meterToLatitudeMin(dist: dist, lat: lastLatitude)
        let latMax = meterToLatitudeMax(dist: dist, lat: lastLatitude)
        let longMin = meterToLongitudeMin(dist: dist, lng: lastLongitude, lat: lastLatitude)
        let longMax = meterToLongitudeMax(dist: dist, lng: lastLongitude, lat: lastLatitude)
        let notes = realm.objects(Note.self).filter("latitude >= \(latMin) AND latitude <= \(latMax) AND longitude >= \(longMin) AND longitude <= \(longMax) ").sorted(byKeyPath: "dateCreate", ascending: true)
        mapView.clear()
        var i:Int32 = 1;
        notes.forEach({(note) in
            if isInRadius(myLat: lastLatitude, myLong: lastLongitude, lat: note.latitude, long: note.longitude, dist: dist) {
                let type:NoteMarker.MarkerType
                if note.username == UserDefaults.standard.string(forKey: "username") {
                    type = .mine
                } else {
                    type = .foreign
                }
                let marker = NoteMarker(type: type,id: note.id!, latitude: note.latitude, longitude: note.longitude)
                marker.zIndex = i
                i+=1
                marker.map = mapView
            }
        })
        
        let circleCenter = CLLocationCoordinate2D(latitude: lastLatitude, longitude: lastLongitude)
        let circ = GMSCircle(position: circleCenter, radius: dist)
        circ.strokeColor = #colorLiteral(red: 0.8650252819, green: 0.393302083, blue: 0.4146752357, alpha: 0.8470588235)
        circ.strokeWidth = 1
        circ.map = mapView
    }
    
    func rad2deg(_ number: Double) -> Double {
        return number * 180.0 / .pi
    }
    
    func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180.0
    }
    
    fileprivate func meterToLatitudeMax(dist: Double, lat: Double) -> Double {
        let radius = 6371000.0;
        return lat + rad2deg(dist / radius)
    }
    
    fileprivate func meterToLatitudeMin(dist: Double, lat: Double) -> Double {
        let radius = 6371000.0;
        return lat - rad2deg(dist / radius)
    }
    
    fileprivate func meterToLongitudeMax(dist: Double, lng: Double, lat: Double) -> Double {
        let radius = 6371000.0;
        return lng + rad2deg(dist / radius / cos(deg2rad(lat)))
    }
    
    fileprivate func meterToLongitudeMin(dist: Double, lng: Double, lat: Double) -> Double {
        let radius = 6371000.0;
        return lng - rad2deg(dist / radius / cos(deg2rad(lat)))
    }
    
    fileprivate func isInRadius(myLat: Double, myLong: Double, lat: Double, long: Double, dist: Double) -> Bool {
        let radius = 6371000.0;
        
        return acos(sin(deg2rad(myLat)) * sin(deg2rad(lat)) + cos(deg2rad(myLat)) * cos(deg2rad(lat)) * cos(deg2rad(myLong) - deg2rad(long))) * radius < dist
    }
    
    fileprivate func getNotes(completion: @escaping ([Note]?, Error?) -> ()) {
        var url = URLComponents(string: "http://\(URLHelper.getPrimaryUrl()):8080/app/getnotes")!
        
        url.queryItems = [
            URLQueryItem(name: "latitude", value: String(lastLatitude)),
            URLQueryItem(name: "longitude", value: String(lastLongitude))
        ]
        var request = URLRequest(url: url.url!)
        request.httpMethod = "GET"
        
        let username = UserDefaults.standard.string(forKey: "username") ?? ""
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
        let loginString = String(format: "%@:%@", username, token)
        let loginData = loginString.data(using: .utf8)!
        let base64LoginString = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        URLSession.shared.dataTask(with: request, completionHandler: {(data, resp, err) in
            if let err = err {
                completion([], err)
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601withFractionalSeconds
                let notes = try decoder.decode([Note].self, from: data!)
                completion(notes, nil);
            } catch let jsonError {
                completion(nil, jsonError)
            }
        }).resume()
    }
    
    @objc func updateNotes() {
        DispatchQueue.main.async() {
            self.getNotes(completion: {(notes, err) in
                if let err = err {
                    print("Failed getting notes: ", err)
                    return
                }
                
                let realm = try! Realm();
                
                notes?.forEach({(note) in
                    if self.isInRadius(myLat: self.lastLatitude, myLong: self.lastLongitude, lat: note.latitude, long: note.longitude, dist: self.dist) {
                        try! realm.write {
                            realm.add(note, update: .modified)
                        }
                    }
                })
                
                try! realm.write {
                    realm.add(notes!, update: .modified)
                }
            })
            self.drawMarkers()
        }
    }
    
    func checkLocationAuthorization() {
        let status = CLLocationManager.authorizationStatus()
        
        if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()){
            let alert = UIAlertController(title: "Upozornění", message: "Musíte povolit polohové údaje", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction) in
                self.checkLocationAuthorization()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if(status == .notDetermined){
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func checkLocationAuthorizationAndUpdating() {
        checkLocationAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide;
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.isAlreadyAppeared {
            drawMarkers()
        }
        isAlreadyAppeared = true
    }
}

extension MapController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorizationAndUpdating()
    }
    
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count == 1 {
            if let location = locations.last?.coordinate {
                lastLatitude = location.latitude
                lastLongitude = location.longitude
                mapView.animate(to: GMSCameraPosition(latitude: location.latitude, longitude: location.longitude, zoom: 16))
            }
            drawMarkers()
            return
        }
        if let location = locations.last?.coordinate {
            lastLatitude = location.latitude
            lastLongitude = location.longitude
            mapView.animate(to: GMSCameraPosition(latitude: location.latitude, longitude: location.longitude, zoom: 16))
        }
        
        drawMarkers()
    }
}

extension MapController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker;
        let data:Dictionary<String, Any> =  marker.userData as! Dictionary<String, Any>
        
        let noteController = self.storyboard?.instantiateViewController(withIdentifier: "NoteController") as! NoteController
        noteController.idNote = data["id"] as! String
        noteController.modalPresentationStyle = UIModalPresentationStyle.popover
        noteController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        noteController.popoverPresentationController?.delegate = self
        
        self.present(noteController, animated: true, completion: nil)
        
        return true
    }
}

extension MapController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}

extension JSONDecoder.DateDecodingStrategy {
    static let iso8601withFractionalSeconds = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        guard let date = Formatter.iso8601.date(from: string) else {
            throw DecodingError.dataCorruptedError(in: container,
                                                   debugDescription: "Invalid date: " + string)
        }
        return date
    }
}

