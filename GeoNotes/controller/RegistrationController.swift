//
//  LoginController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 17/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit

class RegistrationController: UIViewController {
    
    struct User: Decodable {
        let username: String
        let token: String
    }
    
    @IBOutlet weak var usernameTextField: UsernameTextField!
    
    @IBAction func registrationHandler(_ sender: Any) {
        let username = usernameTextField.text ?? ""
        if username.isEmpty {
            let alert = UIAlertController(title: "Upozornění", message: "Vypňtě uživatelské jménono bez mezer", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        } else {
            postUsername(username: username, completion: {(user, err) in
                if let err = err {
                    print("Failed username post: ", err)
                    return
                }
                
                UserDefaults.standard.set(user?.username, forKey: "username")
                UserDefaults.standard.set(user?.token, forKey: "token")
            })
            let mapController = self.storyboard?.instantiateViewController(withIdentifier: "GeoNoteNavigatonController") as! GeoNoteNavigatonController
            self.topMostController().present(mapController, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    fileprivate func postUsername(username: String, completion: @escaping (User?, Error?) -> ()) {
        var request = URLRequest(url: URL(string: "http://\(URLHelper.getPrimaryUrl()):8080/nicetomeetyou")!)
        request.httpMethod = "POST"
        request.httpBody = "username=\(username)".data(using: .utf8)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: request, completionHandler: {(data, resp, err) in
            if let err = err {
                completion(nil, err)
                return
            }
            do {
                let user = try JSONDecoder().decode(User.self, from: data!)
                completion(user, nil);
            } catch let jsonError {
                completion(nil, jsonError)
            }
        }).resume()
    }
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    
}
