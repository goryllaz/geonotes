//
//  NotesController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 27/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import RealmSwift

class NotesController: UIViewController {
    
    fileprivate var notes: Results<Note>?
    
    enum NotesType {
        case mine
        case foreign
    }
    
    @IBOutlet var notesSwitch: UISegmentedControl!
    @IBOutlet var notesNavBar: UINavigationBar!
    @IBOutlet var notesTable: UITableView!
    @IBOutlet var notesBackButton: UIButton!
    
    
    
    @IBAction func notesCloseHandler(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func changeTypeHandler(_ sender: Any) {
        if notesSwitch.selectedSegmentIndex == 0 {
            updateData(type: .foreign)
            notesBackButton.setTitleColor(#colorLiteral(red: 0.8650252819, green: 0.393302083, blue: 0.4146752357, alpha: 0.8470588235), for: .normal)
            notesSwitch.tintColor = #colorLiteral(red: 0.8650252819, green: 0.393302083, blue: 0.4146752357, alpha: 0.8470588235)
        } else {
            updateData(type: .mine)
            notesBackButton.setTitleColor(#colorLiteral(red: 0.1764705882, green: 0.6980392157, blue: 0.6588235294, alpha: 1), for: .normal)
            notesSwitch.tintColor = #colorLiteral(red: 0.1764705882, green: 0.6980392157, blue: 0.6588235294, alpha: 1)
        }
        notesTable.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notesTable.delegate = self
        notesTable.dataSource = self
        updateData(type: .foreign)
    }
    
    override func viewDidLayoutSubviews() {
        notesNavBar.frame = CGRect(x: notesNavBar.frame.minX, y: UIApplication.shared.statusBarFrame.height, width: notesNavBar.frame.width, height: notesNavBar.frame.height)
        notesTable.frame.origin = CGPoint(x: notesTable.frame.minX, y: notesNavBar.frame.maxY)
        notesTable.frame.size = CGSize(width: notesTable.frame.width, height: view.frame.height - notesNavBar.frame.maxY)
    }
    
    fileprivate func updateData(type: NotesType) {
        let realm = try! Realm()
        let username = UserDefaults.standard.string(forKey: "username")!
        var filterString:String {
            switch type {
            case .mine:
                return "username == '\(username)'"
            case .foreign:
                return "username != '\(username)'"
            }
        }
        notes = realm.objects(Note.self).filter(filterString).sorted(byKeyPath: "dateCreate")
    }
}

extension NotesController: UITableViewDataSource, UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = notes![indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as! NoteCell
        cell.setNoteCell(note: note)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noteController = self.storyboard?.instantiateViewController(withIdentifier: "NoteController") as! NoteController
        noteController.idNote = notes![indexPath.row].id!
        noteController.modalPresentationStyle = UIModalPresentationStyle.popover
        noteController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        noteController.popoverPresentationController?.delegate = self
        tableView.deselectRow(at: indexPath, animated: true)
        self.present(noteController, animated: true, completion: nil)
    }
    
}

extension NotesController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
