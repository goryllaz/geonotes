//
//  NoteController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 26/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleMaps

class NoteController: UIViewController {
    
    var note: Note?
    
    @IBOutlet var noteNavBar: NoteNavigationBar!
    @IBOutlet var noteTextView: UITextView!
    @IBOutlet var noteUsername: UILabel!
    @IBOutlet var noteScrollView: UIScrollView!
    @IBOutlet var noteMap: GMSMapView!
    @IBOutlet var noteDate: UILabel!
    
    
    @IBAction func cancelButtonHandler(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var idNote: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteNavBar.delegate = self
        
        let realm = try? Realm()
        
        let note = realm?.objects(Note.self).filter("id = '\(idNote)'").first
        
        self.note = note
        
        noteTextView.text = note?.text
        noteUsername.text = note?.username
        let datef = DateFormatter()
        datef.dateFormat = "HH:mm dd.MM.YYYY"
        noteDate.text = datef.string(from: note!.dateCreate!)
    
        noteTextView.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        noteTextView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        let fixedWidth = noteTextView.frame.size.width
        let newSize = noteTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        noteTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        noteTextView.isScrollEnabled = false
        noteTextView.frame.origin = CGPoint(x: noteTextView.frame.minX, y: 0)
        noteUsername.frame.origin = CGPoint(x: 8, y: noteTextView.frame.maxY + 15)
        noteMap.frame.origin = CGPoint(x: noteMap.frame.minX, y: noteUsername.frame.maxY + 15)
        noteDate.frame.origin = CGPoint(x: noteDate.frame.minX, y: noteMap.frame.maxY + 15)
        
        if note?.username == UserDefaults.standard.string(forKey: "username") {
            noteNavBar.barTintColor = UIColor(red: 45.0/255.0, green: 178.0/255.0, blue: 168.0/255.0, alpha: 1.0)
            noteUsername.textColor = UIColor(red: 45.0/255.0, green: 178.0/255.0, blue: 168.0/255.0, alpha: 1.0)
        }
    }
    
    override func viewDidLayoutSubviews() {
        noteNavBar.frame = CGRect(x: noteNavBar.frame.minX, y: UIApplication.shared.statusBarFrame.height, width: noteNavBar.frame.width, height: noteNavBar.frame.height)
        noteScrollView.frame.origin = CGPoint(x: noteScrollView.frame.minX, y: noteNavBar.frame.maxY)
        noteScrollView.frame.size = CGSize(width: noteScrollView.frame.width, height: view.frame.height - noteNavBar.frame.maxY)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        noteMap.camera = GMSCameraPosition(latitude: note!.latitude, longitude: note!.longitude, zoom: 16)
        let type:NoteMarker.MarkerType
        if note!.username == UserDefaults.standard.string(forKey: "username") {
            type = .mine
        } else {
            type = .foreign
        }
        let marker = NoteMarker(type: type,id: note!.id!, latitude: note!.latitude, longitude: note!.longitude)
        marker.map = noteMap
    }
}

extension NoteController: UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
