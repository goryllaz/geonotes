//
//  AddNoteController.swift
//  GeoNotes
//
//  Created by Jan Urbanec on 24/06/2019.
//  Copyright © 2019 Jan Urbanec. All rights reserved.
//

import UIKit
import RealmSwift

class AddNoteController: UIViewController {
    
    @IBOutlet var AddNoteNavBar: UINavigationBar!
    @IBOutlet var noteTextView: UITextView!
    
    struct NotePOST: Codable {
        let text: String
        let latitude: Double
        let longitude: Double
    }
    
    @IBAction func cancelButtonHandler(_ sender: Any) {
        noteTextView.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonHandler(_ sender: Any) {
        let note = NotePOST(text: noteTextView.text, latitude: UserDefaults.standard.double(forKey: "lastAddLatitude"), longitude: UserDefaults.standard.double(forKey: "lastAddLongitude"))
        postNote(note: note, completion: {(responseNote, err) in
            if let err = err {
                print("Failed adding note: ", err)
                return
            }
            
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(responseNote!)
            }
        })
        
        noteTextView.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShowAndChangeHeightTextView),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        let startPosition: UITextPosition = noteTextView.beginningOfDocument
        noteTextView.selectedTextRange = noteTextView.textRange(from: startPosition, to: startPosition)
        noteTextView.becomeFirstResponder()
    }
    
    fileprivate func postNote(note: NotePOST, completion: @escaping (Note?, Error?) -> ()) {
        var request = URLRequest(url: URL(string: "http://\(URLHelper.getPrimaryUrl()):8080/app/addnote")!)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONEncoder().encode(note)
        } catch {
            completion(nil, error)
        }
        let username = UserDefaults.standard.string(forKey: "username") ?? ""
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
        let loginString = String(format: "%@:%@", username, token)
        let loginData = loginString.data(using: .utf8)!
        let base64LoginString = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: request, completionHandler: {(data, resp, err) in
            if let err = err {
                completion(nil, err)
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601withFractionalSeconds
                let note = try decoder.decode(Note.self, from: data!)
                completion(note, nil);
            } catch let jsonError {
                completion(nil, jsonError)
            }
        }).resume()
    }
    
    @objc func keyboardWillShowAndChangeHeightTextView(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let y:CGFloat = AddNoteNavBar.frame.maxY
            let width:CGFloat = view.frame.width
            let height:CGFloat = view.frame.height - ( AddNoteNavBar.frame.maxY + keyboardHeight )
            noteTextView.frame = CGRect(x: 0, y: y, width: width, height: height)
            noteTextView.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
            noteTextView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        }
    }
    
    override func viewDidLayoutSubviews() {
        AddNoteNavBar.frame = CGRect(x: AddNoteNavBar.frame.minX, y: UIApplication.shared.statusBarFrame.height, width: AddNoteNavBar.frame.width, height: AddNoteNavBar.frame.height)
        
        let y:CGFloat = AddNoteNavBar.frame.maxY
        let width:CGFloat = view.frame.width
        let height:CGFloat = view.frame.height - AddNoteNavBar.frame.maxY
        noteTextView.frame = CGRect(x: 0, y: y, width: width, height: height)
        noteTextView.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        noteTextView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
    }
}
